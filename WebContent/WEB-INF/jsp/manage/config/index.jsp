<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/ibpdTag" prefix="ibpd"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>系统设置</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/jquery.datetimepicker.css" />
		<style type="text/css">
			.oper {
				position:absolute;
			    width:100px;
			    height:100px;
			    text-align:right;
			    right:15px;	
			    top:40px;	
			    z-index:10000;	
			}
			.tabs {
				float:left;
			    width:100%;
			}
			.tabs ul 
			{
			    list-style: none outside none;
			    margin: 0;
			    padding: 0;
			}
			.tabs ul li
			{
			    float: left;
			    line-height: 24px;
			    margin: 0;
			    margin-left:1px;
			    padding: 2px 20px 0 15px;
			}
			.tab-nav{
				 cursor:pointer;
				 border: 1px solid #94a6d3;
				 background-color:#e0e0e0;
				 -webkit-border-top-left-radius: 5px;
				 -webkit-border-top-right-radius: 5px;
				 -moz-border-radius: 5px; 
				 text-align: -webkit-match-parent;
			}
			.tab-nav-action{
				cursor:pointer;
				border: 1px solid #8DB2E3; 
				background-color:#848484;
				-webkit-border-top-left-radius: 5px;
				-webkit-border-top-right-radius: 5px;
				-moz-border-radius: 5px; 
				text-align: -webkit-match-parent;
			}
			.tabs-body
			{
			    border-bottom: 1px solid #B4C9C6;
			    border-left: 1px solid #B4C9C6;
			    border-right: 1px solid #B4C9C6;
			    float: left;
			    padding: 5px 0 0;
			    width: 100%;
			}
			.tabs-body div 
			{
			    padding:10px;
			}
			.tabs-body table{
				width:90%;
			}
			.tabs-body table .tit{
				width:20%;
				overflow:block;
				font-size:12px;
			}
			.tabs-body table .val{
				width:30%;
			}
			.tabs-body table .val span{
			}
			.tabs-body table .val span input{
				width:100%;
				font-size:12px;
			}
			.tabs-body table .val span select{
				width:100%;
				font-size:12px;
			}
			.tabs-body table .rem{
				width:40%;
				padding-left:20px;
				font-size:12px;
			}
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	
	
	</head>

	<body>
	
	
	
	<div class="easyui-layout" fit="true">
		<div region="west" border="true">
		<div class="datagrid-toolbar"><table cellspacing="0" cellpadding="0"><tbody><tr><td><a href="javascript:void(0)" class="l-btn l-btn-small l-btn-plain" group="" id="btnSave"><span class="l-btn-left l-btn-icon-left"><span class="l-btn-text">保存设置</span><span class="l-btn-icon icon-save">&nbsp;</span></span></a></td></tr></tbody></table></div>
		
		<div class="tabs">
        <ul id="tabs">
            <li class="tab-nav-action">网站基础信息</li>
            <li class="tab-nav">网站高级设置</li>
            <li class="tab-nav">站点静态设置</li>
            <li class="tab-nav">站点备份设置</li>
            <li class="tab-nav">数据库连接和备份</li>
            <li class="tab-nav">图片转换设置</li>
            <li class="tab-nav">图片水印设置</li> 
            <li class="tab-nav">其他</li>
        </ul>
    </div>
    <div id="tabs-body" class="tabs-body">
        <div style="display:block">
        <%@include file="base.jsp" %>
        </div>
        <div style="display:none">
        <%@include file="senior.jsp" %>
        </div>
        <div style="display:none">
        <%@include file="static.jsp" %>
        </div>
        <div style="display:none">
        <%@include file="bak.jsp" %>
         </div>
        <div style="display:none">
         <%@include file="db.jsp" %>
        </div>
        <div style="display:none">
        <%@include file="image.jsp" %>
        </div>
        <div style="display:none">
        <%@include file="mask.jsp" %>
        </div>
        <div style="display:none">
        </div>
    </div>
		</div>
	</div>
	</body>
		<script src="<%=basePath %>js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
        $(document).ready(function () {
            $("#tabs li").bind("click", function () {
                var index = $(this).index();
                var divs = $("#tabs-body > div");
                $(this).parent().children("li").attr("class", "tab-nav");//将所有选项置为未选中
                $(this).attr("class", "tab-nav-action"); //设置当前选中项为选中样式
                divs.hide();//隐藏所有选中项内容
                divs.eq(index).show(); //显示选中项对应内容
            });
            $("#weeks").hide();
		$('#siteOpenTime_val').datetimepicker();
        });
	
       var logic = function( currentDateTime ){
	if( currentDateTime.getDay()==6 ){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
	};
 		$('#open_month_days_val').datetimepicker({datepicker:false,format:'H:i',step:5,lang:'ch'});
 		$('#open_week_days_val').datetimepicker({datepicker:false,format:'H:i',step:5,lang:'ch'});
 		$('#open_evaryday_val').datetimepicker({datepicker:false,format:'H:i',step:5,lang:'ch'});
 		$('#open_singDate_val').datetimepicker({step:5,minDate:'2014/08/01',maxDate:'2014/08/31',lang:'ch',onChangeDateTime:logic});
		$('#close_month_days_val').datetimepicker({datepicker:false,format:'H:i',step:5,lang:'ch'});
 		$('#close_week_days_val').datetimepicker({datepicker:false,format:'H:i',step:5,lang:'ch'});
 		$('#close_evaryday_val').datetimepicker({datepicker:false,format:'H:i',step:5,lang:'ch'});
 		$('#close_singDate_val').datetimepicker({step:5,minDate:'2014/08/01',maxDate:'2014/08/31',lang:'ch',onChangeDateTime:logic});
 		$(".open_tr").hide();
 		$(".close_tr").hide();
 		$("#timmingOpen_select").get(0).selectedIndex=1;
 		$("#timmingClose_select").get(0).selectedIndex=1;
 		fillOption($("#open_week_day_select"),7);
 		fillOption($("#close_week_day_select"),7);
 		fillOption($("#open_month_days_select"),31);
 		fillOption($("#close_month_days_select"),31);
 		function swap(obj){
	 			var _tmp=$("#"+$(obj).attr('id')+" option:selected").attr('id');
	 		if($(obj).attr('id')=='timmingOpen_select'){
	 			if(_tmp=='1'){
	 				$(".open_tr").show();
	 			}else{
	 				$(".open_tr").hide();
	 			}
	 			
	 		}else if($(obj).attr('id')=='timmingClose_select'){
	 			if(_tmp=='1'){
	 				$(".close_tr").show();
	 			}else{
	 				$(".close_tr").hide();
	 			}
	 		}
 		}
 		function fillOption(obj,leng){
	 		for(i=1;i<=leng;i++){
	 			obj.append("<option id=\""+i+"\">"+i+"</option>");
	 		}
 			
 		}
    </script>

</html>
