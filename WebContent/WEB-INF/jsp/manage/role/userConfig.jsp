<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
	<div id="top" region="north" style="background: #eee;height:40px;padding:5px">
		<a href="#" id="btn_submit" icon="icon-search">确认保存</a>
		<a href="#" id="btn_cancel" icon="icon-search">取消关闭</a>
		<a href="#" id="btn_viewAll" icon="icon-search">显示所有用户</a>
		<a href="#" id="btn_viewSelected" icon="icon-search">显示已选择用户</a>
	</div>
    <div id="nodeview" region="center" style="background: #eee; overflow-y:hidden">
        <div id="tabs" tabPosition="bottom" class="easyui-tabs"  fit="true" border="false" >
			<div title="角色-用户管理" style="padding:0;overflow:hidden; color:red; " >
			     <table id="grid" style="width: 900px;height:auto;" title="角色-用户管理" iconcls="icon-view">            
           		 </table>
			</div>
		</div>
    </div>
	</div>
	
	<script type="text/javascript">
	var queryData={};
	$(document).ready(function(){
		InitGrid(queryData);
		$('#btn_submit').linkbutton({
			iconCls:'icon-ok'
		});
		$('#btn_cancel').linkbutton({
			iconCls:'icon-cancel'
		});
		$('#btn_viewAll').linkbutton({
			iconCls:'icon-search'
		});
		$('#btn_viewSelected').linkbutton({
			iconCls:'icon-search'
		});
		$("#btn_submit").click(function(){
				var _sets=$('#grid').datagrid("getSelections");
				if(_sets.length==0){
					submitUserInfo("-1");
				}else{
					var _ids="";
					$.each(_sets,function(i,n){
						_ids+=n.userId+",";
						submitUserInfo(_ids);
					});			
				}
			});
			
		$('#btn_cancel').click(function(){
			closeCurWindow();
		});
		$('#btn_viewAll').click(function(){	
			url=url.replace("displayRoleUserInfo=0","displayRoleUserInfo=1");
			InitGrid(queryData);
		});
		$('#btn_viewSelected').click(function(){
			url=url.replace("displayRoleUserInfo=1","displayRoleUserInfo=0");
			InitGrid(queryData);
		});
	});
      
	function submitUserInfo(ids){
			$.ajax({
				 type: "POST",
				 url: "<%=basePath %>/Manage/Role/doRoleUserInfo.do?roleId=${roleId}",
				 data: {userIds:ids},
				 dataType: "text",
				 async:false,
				 success: function(result){
					if(result.indexOf("msg")!=0){
						var t=eval("("+result+")");
						if(t.status='99'){
							closeCurWindow();
						}else{
							alert(t.msg);
						}
					}
				}
			 });
	};
 	function closeCurWindow(){ 
		window.opener='';
		window.open('','_self');
		window.close();
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#grid').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#grid').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#grid').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#grid').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
	   var url='<%=basePath %>Manage/Role/getRoleUserInfo.do?roleId=${roleId}&displayRoleUserInfo=0&t='+new Date();
      function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: url, 
                title: '',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			//e.preventDefault();
        			//$('#grid').datagrid('uncheckAll');
                   // $('#grid').datagrid('checkRow', rowIndex);
				   // $('#nodeCtxMenu').menu('show', {
				   //     left:e.pageX,
				   //     top:e.pageY
				   // });    
   				},
   				rowStyler:function(index,row){
   					//if(row.state<=0){
   					//return "background-color:RGB(79,129,189);";
   					//}else{
   					//}
   				},
                columns: [[
                    { field: 'ck', checkbox: true,title:'选择' },
					{ title: 'ID', field: 'id', width: 30, sortable:true,hidden:true },
					{ title: '用户ID', field: 'userId', width: 50, sortable:true,hidden:true },
 					{ title: '用户名', field: 'userName', width: 130, sortable:true},
					{ title: '角色Id', field: 'roleId', width: 50, sortable:true,hidden:true }
              ]], 
               onDblClickRow: function (rowIndex, rowData) {
               },
               onClickRow: function (rowIndex, rowData) {
               }
            })
        };
 			
     </script>
	</body>
</html>
