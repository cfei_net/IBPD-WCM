<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/default/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/poshytip-1.2/src/tip-yellow/tip-yellow.css" />
		<style type="text/css">
		#specTable{
			width:100%;
		}
		#specTable .specTitle{
		}
		#specTable .specTitle th{
			width:20%;
			text-align:center;
		}
		#specTable .specTitle th *{
			
		}
		#specTable .specBody{
		}
		#specTable .specBody td{
			text-align:center;
		}
		#specTable .specBody .scolor{
			width:20%;
		}
		#specTable .specBody .scolor input{
			width:80%;
		}
		#specTable .specBody .ssize{
			width:20%;
		}
		#specTable .specBody .ssize input{
			width:80%;
		}
		#specTable .specBody .stock{
			width:20%;
		}
		#specTable .specBody .stock input{
			width:80%;
		}
		#specTable .specBody .price{
			width:20%;
		}
		#specTable .specBody .price input{
			width:80%;
		}
		#specTable .specBody .status{
			width:20%;
		}
		#specTable .specBody .status input{
			width:80%;
		}
		</style> 
	<script type="text/javascript">var contentId=-1;</script>
     <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.validatebox.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/extValidatebox.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/poshytip-1.2/src/jquery.poshytip.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script src="<%=basePath%>js/ckeditor/ckeditor.js"></script>
	
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
   
    <div data-options="region:'center'"  id="center" region="center" style="background: #eee; overflow-y:hidden;height:400px;">
	<div class="easyui-tabs"  fit="true" border="false" id="tabs">
	<div title="基本信息">
	<c:if test="${type=='add'}">
		<input name="product.catalogId" type="hidden" value="${cataId}" mthod="smt"/>
	</c:if>
	<c:if test="${type=='edit'}">
		<input name="product.id" type="hidden" value="${entity.id}" mthod="smt"/>
		<input name="product.catalogId" type="hidden" value="${entity.catalogId}" mthod="smt"/>
		<input name="product.tenantId" type="hidden" value="${entity.tenantId}" mthod="smt"/>
		<input name="product.status" type="hidden" value="${entity.status}" mthod="smt"/>
	</c:if>
    	<table cellpadding="0" cellspacing="0" border="0">
		<c:forEach items="${htmls }" var="html">
			<c:if test="${html.value.k=='base'}">
			<tr>
				<td class="tit">
					<div class="tittext">${html.key }</div>
				</td>
				<td class="val">
					${html.value.v }
				</td>		
			</tr>
			</c:if>
		</c:forEach>
		</table>
    </div>
	<div title="商品规格">
		
    	<table cellpadding="0" cellspacing="0" border="0" id="specTable">
			<tr class="specTitle">
				<th>颜色</th>
				<th>尺寸</th>
				<th>库存</th>
				<th>价格</th>
				<!--th>启用</th-->
				<th>操作</th>
			</tr>
			<c:if test="${type=='add'}">
			<tr class="specBody">
				<td class="scolor">
					<input type="text" mthod="smt" name="specList[0].specColor"/>
				</td>
				<td class="ssize">
					<input type="text" mthod="smt" name="specList[0].specSize"/>
				</td>
				<td class="stock">
					<input type="text" mthod="smt" name="specList[0].specStock"/>
				</td>
				<td class="price">
					<input type="text" mthod="smt" name="specList[0].specPrice"/>
				</td>
				<td class="status">
					<!--input type="text" mthod="smt" name="specList[0].specStatus"/-->
					<input type="button" class="addNewSpec" value="新增"/>
				</td>
			</tr>
			</c:if>
			<c:if test="${type=='edit'}">
			<c:forEach items="${specList }" var="spec" varStatus="status_spec">
			<tr class="specBody">
				<td class="scolor">
					<input type="hidden" mthod="smt" name="specList[${status_spec.index}].id" value="${spec.id}"/>
					<input type="hidden" mthod="smt" name="specList[${status_spec.index}].productId" value="${spec.productId}"/>
					<input type="hidden" mthod="smt" name="specList[${status_spec.index}].specStatus" value="${spec.specStatus}"/>
					<input type="text" mthod="smt" name="specList[${status_spec.index}].specColor" value="${spec.specColor}"/>
				</td>
				<td class="ssize">
					<input type="text" mthod="smt" name="specList[${status_spec.index}].specSize" value="${spec.specSize}"/>
				</td>
				<td class="stock">
					<input type="text" mthod="smt" name="specList[${status_spec.index}].specStock" value="${spec.specStock}"/>
				</td>
				<td class="price">
					<input type="text" mthod="smt" name="specList[${status_spec.index}].specPrice" value="${spec.specPrice}"/>
				</td>
				<td class="status">
					<!--input type="text" mthod="smt" name="specList[${status_spec.index}].specStatus" value="${spec.specStatus}"/-->
					<input type="button" class="addNewSpec btn_new_${status_spec.index}" value="新增"/>
				</td>
			</tr>
			</c:forEach>
			</c:if>
		</table>
	</div>
	<div title="商品参数">
    	<table cellpadding="0" cellspacing="0" border="0">
	<c:if test="${type=='add'}">
		<c:forEach items="${attrList }" var="attr" varStatus="status_attr">
			<tr>
				<td class="tit">
					<div class="tittext">${attr.attrName }</div>
				</td>
				<td class="val">
					<input type="hidden" mthod="smt" name="attrList[${status_attr.index}].attrId" value="${attr.id}"/>
					<input type="text" mthod="smt" name="attrList[${status_attr.index}].value" value=""/>
				</td>
			</tr>
		</c:forEach>		
	</c:if>
	<c:if test="${type=='edit'}">
		<c:forEach items="${attrList }" var="attr" varStatus="status_attr">
			<tr>
				<td class="tit">
					<div class="tittext">${attr.attrName }</div>
				</td>
				<td class="val">
					<input type="hidden" mthod="smt" name="attrList[${status_attr.index}].id" value="${attr.id}"/>
					<input type="hidden" mthod="smt" name="attrList[${status_attr.index}].attrId" value="${attr.attrId}"/>
					<input type="hidden" mthod="smt" name="attrList[${status_attr.index}].productId" value="${attr.productId}"/>
					<input type="text" mthod="smt" name="attrList[${status_attr.index}].value" value="${attr.attrValue}"/>
				</td>
			</tr>
		</c:forEach>
		
	</c:if>
		</table>
    </div>
	<div title="商品介绍">
   	 <textarea id="text">${entity.productHTML }</textarea>
	 </div>
    </div>
    </div>
   <div id="top" data-options="region:'north'" region="north" style="background: #eee; overflow-y:hidden">
    <div class="datagrid-toolbar">
    	<table cellspacing="0" cellpadding="0">
    		<tbody>
    			<tr>
    				<td>
    					<a href="javascript:void(0)" class="l-btn l-btn-small l-btn-plain" group="" id="save">
    					<span class="l-btn-left l-btn-icon-left"><span class="l-btn-text">保存</span>
    						<span class="l-btn-icon icon-add">&nbsp;</span>
    					</span>
    					</a>
    				</td>
     				<td>
    					
    				<br></td>
    				<td>
    					<a href="javascript:void(0)" class="l-btn l-btn-small l-btn-plain" group="" id="close"><span class="l-btn-left l-btn-icon-left"><span class="l-btn-text">关闭</span><span class="l-btn-icon icon-remove">&nbsp;</span></span></a>
    				</td>
    			</tr>
    		</tbody>
    	</table>
    </div>
    </div>
	</div>

	<script type="text/javascript">
	var path="<%=path %>";
	var textEditor=null;
	var process=null;
	var type="${type }";
	var nodeId="${cataId }";
	var specSize=1;
	if(type=='edit'){
		nodeId="${entity.id }";
		<c:if test="${specList!=null}">
		specSize=${fn:length(specList)};
		</c:if>
		//specSize++;
	};
	function getsp(s){
	return s.split(":");
	};
		function bindEvents(){
		$("input[smt='formatter']").click(function(e){parent.showFormatterDialog(e);});
		$("input[smt='fileSelecter']").click(function(e){selectFile(e);});
		$("input[smt='userSelecter']").click(function(e){parent.showUserSelecterDialog(e);});
		$("input[smt='templateSelecter']").click(function(e){parent.showTemplateSelecter(e);});
		$("input[smt='dateSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='timeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='dateTimeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='colorSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
	};	
	//由于本页面是嵌套在IFRAME中的，所以在展现一些对话框时，无法显示到本页（被挡住了，放不下），所以需要 调用上级页面的对话框显示函数，然后函数再调用下面的这个方法设置值
	/**
	*
	**/
	function setValue(name,val){
		$("[name='"+name+"']").val(val);
		$("[name='"+name+"']").blur();
	};

	$(document).ready(function(){
		textEditor=CKEDITOR.replace('text');
		//textEditor.on("instanceReady",function(){textEditor.setData("${text }");});
		initTabs();
		initImageUpload();
		initImages();
		$(".upload").bind("click",{s:this},openUpload);
		$(".clear").bind("click",{s:this},clearUpload);
		$(".addNewSpec").click(function(){addNewSpec();});
		$("#title_btn").bind("click",{s:this},formatTitle);
		$("#save").click(function(){save();});
		$("#close").click(function(){close();});
		$("input[itype='fileSelect']").click(function(e){selectFile(e);});
		bindEvents();
	}); 
	function initImages(){
		var picture=$("input[name='product.picture']");
		var maxPicture=$("input[name='product.maxPicture']");
		var images=$("input[name='product.images']");
		if(picture.length>0){
			var val=picture.val().split(";");
			var i=0;
			for(i=0;i<val.length;i++){
				if(val[i]!=''){
					$(".imageList",picture.parent()).append("<img src=\""+val[i]+"\" width=\"120px\" height=\"100px\" style=\"border:solid black 1px;margin:2 2 2 2\"/>");
				}
			}
		}
		if(maxPicture.length>0){
			var val=maxPicture.val().split(";");
			var i=0;
			for(i=0;i<val.length;i++){
				if(val[i]!=''){
					$(".imageList",maxPicture.parent()).append("<img src=\""+val[i]+"\" width=\"120px\" height=\"100px\" style=\"border:solid black 1px;margin:2 2 2 2\"/>");
				}
			}
		}
		if(images.length>0){
			var val=images.val().split(";");
			var i=0;
			for(i=0;i<val.length;i++){
				if(val[i]!=''){
					$(".imageList",images.parent()).append("<img src=\""+val[i]+"\" width=\"120px\" height=\"100px\" style=\"border:solid black 1px;margin:2 2 2 2\"/>");
				}
			}
		}
	};
	function clearUpload(s){
		$("input[name='"+$(s.target).attr("mthod")+"']").val("");
	};
	var curImageId="";
	function openUpload(s){
		curImageId=$(s.target).attr("mthod");
		window.open(basePath+'/Manage/FileSelecter/index.do?tenantId=${tenantId }','文件上传','toolbar=no,menubar=no,resizable=no,location=no,status=no,z-look:yes');
	};
	function changeFile(s,f){
		if(curImageId=='product.images'){
			var _imgs=$("input[name='"+curImageId+"']").val();
			if($("input[name='"+curImageId+"']").val()==''){
				_imgs='';
			}else{
				_imgs=$("input[name='"+curImageId+"']").val()+';';
			}
			$("input[name='"+curImageId+"']").val(_imgs+s.substring(0,s.length-1));
			$(".imageList",$("input[name='"+curImageId+"']").parent()).append("<img src=\""+s.substring(0,s.length-1)+"\" width=\"120px\" height=\"100px\" style=\"border:solid black 1px;margin:2 2 2 2\"/>");
		}else{
			$("input[name='"+curImageId+"']").val(s);
			$(".imageList",$("input[name='"+curImageId+"']").parent()).html("<img src=\""+s.substring(0,s.length-1)+"\" width=\"120px\" height=\"100px\" style=\"border:solid black 1px;margin:2 2 2 2\"/>");
		}
	};
	function initImageUpload(){
		var picture=$("input[name='product.picture']");
		var maxPicture=$("input[name='product.maxPicture']");
		var images=$("input[name='product.images']");
		if(picture.length>0){
			picture.parent().append("<span><input type=\"button\" class=\"upload\" mthod=\"product.picture\" value=\"上传\"/>&nbsp;&nbsp;<input type=\"button\" class=\"clear\" mthod=\"product.picture\" value=\"清空\"/></span><br/><span class=\"imageList\"></span>")
		}
		if(maxPicture.length>0){
			maxPicture.parent().append("<span><input type=\"button\" class=\"upload\" mthod=\"product.maxPicture\" value=\"上传\"/>&nbsp;<input type=\"button\" class=\"clear\" mthod=\"product.maxPicture\" value=\"清空\"/></span><br/><span class=\"imageList\"></span>")
		}
		if(images.length>0){
			images.parent().append("<span><input type=\"button\" class=\"upload\" mthod=\"product.images\" value=\"上传\"/>&nbsp;<input type=\"button\" class=\"clear\" mthod=\"product.images\" value=\"清空\"/></span><br/><span class=\"imageList\"></span>")
		}
	};
	function addNewSpec(){
		$("<tr class=\"specBody spec_tr_"+specSize+"\"><tr>").appendTo("#specTable");
		$("<td class=\"scolor newSpec_color_"+specSize+"\"></td>").appendTo(".spec_tr_"+specSize);
		$("<td class=\"ssize newSpec_size_"+specSize+"\"></td>").appendTo(".spec_tr_"+specSize);
		$("<td class=\"stock newSpec_stock_"+specSize+"\"></td>").appendTo(".spec_tr_"+specSize);
		$("<td class=\"price newSpec_price_"+specSize+"\"></td>").appendTo(".spec_tr_"+specSize);
		$("<td class=\"status newSpec_status_"+specSize+"\"></td>").appendTo(".spec_tr_"+specSize);
		$("<input type=\"text\" mthod=\"smt\" name=\"specList["+specSize+"].specColor\"/>").appendTo(".newSpec_color_"+specSize);
		$("<input type=\"text\" mthod=\"smt\" name=\"specList["+specSize+"].specSize\"/>").appendTo(".newSpec_size_"+specSize);
		$("<input type=\"text\" mthod=\"smt\" name=\"specList["+specSize+"].specStock\"/>").appendTo(".newSpec_stock_"+specSize);
		$("<input type=\"text\" mthod=\"smt\" name=\"specList["+specSize+"].specPrice\"/>").appendTo(".newSpec_price_"+specSize);
		$("<input type=\"button\" class=\"addNewSpec btn_new_"+specSize+"\" value=\"新增\"/>").appendTo(".newSpec_status_"+specSize);
		$(".btn_new_"+specSize).click(function(){addNewSpec();});
		specSize++;
	};
	function save(){
		//var _s_tmp=$("[data-options]");
		//var flog=true;
		//for(i=0;i<_s_tmp.size();i++){
		//	if(!$(_s_tmp[i]).validatebox("isValid")){
		//		flog=false;
		//		break;
		//	}
		//} 
		//if(!flog){
		//	msgShow("提示","验证错误","error");
		//	return;
		//}
	
		var ss=$("input[mthod]"); 
		var p="";  
		for(s=0;s<ss.length;s++){
			p+="'"+ss[s].name+"':'"+$(ss[s]).val()+"',";
		} 
		if(p.length>0){
			//p="{"+p+"'titleFormat':'"+$("#s_titleFormatParams").val()+"'}");
			p="{"+p+"'titleFormatParams':'"+$("#s_titleFormatParams").val()+"'}";
			process = $.messager.progress({
                title:'稍后',
                msg:'正在保存基本信息,请稍候...'
            });
            var savePath=path+"/Manage/Product/doAdd.do";
            if(type=="edit"){
            	savePath=path+"/Manage/Product/doEdit.do"
            }
			$.post(
				savePath,
				eval("["+p+"]")[0],
				function(result){
					$.messager.progress('close');
					var res=eval("["+result+"]");
					if(res[0].status=='99'){
						process = $.messager.progress({
	                		title:'稍后',
	                		msg:'正在保存正文,请稍候...'
	           			});
						saveContent(res[0].id);
					}else{
						msgShow("提示",res[0].msg+"<br/>错误代码:"+res[0].status,"error");
					}
				}
			); 
		}
	};
	function saveContent(id){
			$.post(
				path+"/Manage/Product/saveText.do",
				{id:id,content:textEditor.getData()},
				function(result){
					$.messager.progress('close');
					var res=eval("["+result+"]");
					if(res[0].status=='99'){
						window.opener.reload();
						 $.messager.confirm("提示","保存成功,是否继续新建文档?",function(r){if(r){newDoc();}else{closeCurWindow();}});
					}else{
						msgShow("提示",res[0].msg+"<br/>错误代码:"+res[0].status,"error");
					}
				}
			); 
	};
	function newDoc(){ 
		//var ss=$("input[mthod]"); 
		//for(i=0;i<ss.length;i++){
		//	if($(ss[i]).attr("name")!="nodeId")
		//		$(ss[i]).val("");
		//}
		//textEditor.setData('');
		window.reload();
	}; 
	function closeCurWindow(){ 
		window.opener='';
		window.open('','_self');
		window.close();
	};
	function formatTitle(s){
        var formatDialog = $('<div id="formatTitleDlg"/>').appendTo('body');
      
        $(formatDialog).dialog({
        	modal:true,
        	title:'格式化标题',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:500,
        	height:300,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                    	var fval=$("#formatframe")[0].contentWindow.submit();
                        if(fval!=''){
	                        $(formatDialog).dialog("close");
	                        $(formatDialog).remove();
	                        $("#s_titleFormatParams").val("color:"+fval.curColor+";strong:" + fval.strong+";em:"+fval.em+";u:"+fval.u+";size:"+fval.size);
                        }else{
                        	
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){ 
                        $(formatDialog).dialog("close");
	                    $(formatDialog).remove();
                    }
                }], 
        	content:'<iframe id="formatframe" width="480px" height="230px" scrolling="no" frameborder="no" style="overflow:hidden;" src="<%=basePath %>/Manage/Content/formatTitle.do?txt='+$("#s_title").val()+'"></iframe>'
        }); 
        $(formatDialog).dialog("open");
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
		function initTabs(){
	$("#tabs").tabs({
		onSelect:function(title){
			if(title=='商品介绍'){
				setTimeout(function(){textEditor.resize($("#cke_1_contents").width(),$("#cke_1_contents").parent().parent().parent().height())},20);
			}
			var tab = $('#tabs').tabs('getSelected');
		}
	});
	};

     </script>
	</body>
</html>