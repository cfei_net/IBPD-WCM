package com.ibpd.dao.impl;


import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.metamodel.source.hbm.HibernateMappingProcessor;
import org.hibernate.type.DateType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.IDao;
import com.ibpd.dao.IbpdSessionFactory;
import com.ibpd.dao.impl.HqlParameter.DataType_Enum;
import com.ibpd.entity.baseEntity.IBaseEntity;
@Transactional
@Repository
public class DaoImpl<T extends IBaseEntity>
implements IDao<T>
{
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}

	 
	@Resource(name="mySessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	/* 23 */private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	
	public void delete(Class<T> t, Serializable pk) {
		/* 28 */Object o = getEntity(t, pk);
		/* 29 */if (o == null)
			return;
		Session sesss = getSession();
//		/* 30 */Transaction tx = sesss.beginTransaction();
		/* 31 */getSession().delete(o);
//		/* 32 */tx.commit();
		}

	
	public T getEntity(Class<T> t, Serializable pk) {
		Session s=getSession();
//		Transaction tr=s.beginTransaction();
		T o=(T) getSession().get(t, pk);
//		tr.commit();
		return o;
	}

	
	public List<T> getEntityList(String jpql,
			List<HqlParameter> hqlParameterList, String orderType,
			Integer pageSize, Integer pageIndex)
	{
		/* 41 */if ((orderType != null) && (orderType.length() >= 0)) {
			/* 42 */jpql = jpql + " order by " + orderType;
			}
		Session sesss = getSession();
		/* 46 */Query query = sesss.createQuery(jpql);
		query.setCacheable(true);
		if (hqlParameterList != null) {
			for (HqlParameter hql : hqlParameterList) {
				if (hql.getDataType() != null) {
					DataType_Enum dte = hql.getDataType();
					if (dte == DataType_Enum.Date) {
//						query.setParameter(hql.getParamName(), hql
//								.getParamValue(), Hibernate.DateType.);
						query.setDate(hql.getParamName(), (Date) hql
								.getParamValue());
					} else if (dte == DataType_Enum.TimeSamper) {
//						query.setParameter(hql.getParamName(), hql
//								.getParamValue(), Hibernate.TIMESTAMP);
						query.setTimestamp(hql.getParamName(), (Date) hql
								.getParamValue());
					} else if (dte == DataType_Enum.Boolean) {
//						query.setParameter(hql.getParamName(), hql
//								.getParamValue(), Hibernate.BOOLEAN);
						query.setBoolean(hql.getParamName(), Boolean.valueOf(hql.getParamValue().toString()));
					} else if (dte == DataType_Enum.Integer) {
//						query.setParameter(hql.getParamName(), hql
//						.getParamValue(), Hibernate.INTEGER);
						query.setInteger(hql.getParamName(), Integer.valueOf(hql
								.getParamValue().toString()));
					} else if (dte == DataType_Enum.Long) {
//						query.setParameter(hql.getParamName(), hql
//						.getParamValue(), Hibernate.INTEGER);
						query.setLong(hql.getParamName(), Long.valueOf(hql
								.getParamValue().toString()));
					} else {
						query.setParameter(hql.getParamName(), hql
								.getParamValue());
					}
				} else {
					query.setParameter(hql.getParamName(), hql.getParamValue());
				}
			}
		}
		/* 47 */if (query == null) {
			/* 48 */return null;
			}
		/* 50 */if ((pageSize.intValue() != -1) && (pageIndex.intValue() != -1)) {
			/* 51 */query.setFirstResult(pageIndex.intValue());
			/* 52 */query.setMaxResults(pageSize.intValue());
			}
		List l=query.list();
		return l;
		}

	
	public void save(T t) {
		Session sess = getSession();
		/* 59 */sess.saveOrUpdate(t);
		}

	public void clarCache() {
		getSession().clear();
	}

	public void update(T t) {
		Session sesss = getSession();
		/* 65 */sesss.update(t);
		}

	
	public int executeSqlString(String sql, Class<T> t)
	{
		Session sesss = getSession();
//		/* 71 */Transaction et = sesss.beginTransaction();
		/* 72 */Query query = sesss.createSQLQuery(sql);
		/* 73 */int re = query.executeUpdate();
//		et.commit();
		/* 74 */return re;
		}

	
	public void executeStoreProcedure(String sp)
	{
		Session sesss = getSession();
		/* 79 */Query query = sesss.createQuery("{call " + sp + "}");
		/* 80 */query.executeUpdate();
		}

	
	public List<T> getResultListByCallStoreProctdure(String sp, Class<T> t)
	{
		/* 85 */Query query = getSession().createQuery("{call " + sp + "}");
		/* 86 */return query.list();
		}

	
	public List<T> getSQLResultList(String sql, Class<T> t)
	{
		Session sesss = getSession();
		/* 91 */Query query = sesss.createSQLQuery(sql);
		/* 92 */return query.list();
		}

	
	public int executeSqlString(String sql, Class<T> t, Object[] params) {
		Session sesss = getSession();
		/* 96 */Query query = sesss.createSQLQuery(sql);
		/* 97 */Object[] paras = params;
		/* 98 */for (int i = 1; i <= paras.length; ++i) {
			/* 99 */query.setParameter(i, paras[(i - 1)]);
			}
		/* 101 */return query.executeUpdate();
		}

	
	public List<T> getSQLResultList(String sql, Class<T> t, Object[] params)
	{
		Session sesss = getSession();
		/* 107 */Query query = sesss.createSQLQuery(sql);
		/* 108 */Object[] paras = params;
		/* 109 */for (int i = 1; i <= paras.length; ++i) {
			/* 110 */query.setParameter(i, paras[(i - 1)]);
			}
		/* 112 */return query.list();
		}

	
	public int createTable(Class<T> clazz)
	{
		/* 118 */return 0;
		}

	
	public Object getProperty(String valueField, String whereString)
	{
		/* 123 */Object rtn = null;
		try {
			Session sesss = getSession();
			/* 125 */Query query = sesss.createQuery(whereString);
			query.setCacheable(true);
			/* 126 */List l = query.list();
			/* 127 */if ((l != null) && (l.size() > 0)) {
				/* 128 */Object o = l.get(0);
				/* 129 */Method method = o.getClass().getMethod(
						"get" + valueField.substring(0, 1).trim().toUpperCase()
								+ valueField.substring(1), null);
				/* 130 */rtn = method.invoke(o, null);
				}
			} catch (Exception ex) {
			/* 133 */ex.printStackTrace();
			}
		/* 135 */return rtn;
		}

	
	public List<Object> getPropertyList(String valueField,
			String WhereString)
	{
		/* 140 */List rtn = new ArrayList();
		try {
			Session sesss = getSession();
			/* 142 */Query query = sesss.createQuery(WhereString);
			query.setCacheable(true);
			/* 143 */List obj = query.list();
			/* 144 */Iterator i = obj.iterator();
			/* 145 */while (i.hasNext()) {
				/* 146 */Object o = i.next();
				/* 147 */Method method = o.getClass().getMethod(
						"get" + valueField.substring(0, 1).trim().toUpperCase()
								+ valueField.substring(1), null);
				/* 148 */rtn.add(method.invoke(o, null));
				}
			} catch (Exception ex) {
			/* 151 */ex.printStackTrace();
			}
		/* 153 */return rtn;
		}

	
	public int saveOrUpdate(String qlString, Class<T> t)
	{
		Session sesss = getSession();
		/* 158 */Query query = sesss.createQuery(qlString);
		/* 160 */int re = query.executeUpdate();
		/* 162 */return re;
		}

	
	public Long getItemCount(String entityName, String whereString,
			List<HqlParameter> hqlParameterList)
	{
		/* 167 */String _jpql = "select count(p) from " + entityName + " p "
				+ whereString;
		Session sesss = getSession();
//		Transaction tr=sesss.beginTransaction();
		/* 168 */Query query = sesss.createQuery(_jpql);
		if (hqlParameterList != null) {
			for (HqlParameter hql : hqlParameterList) {
				DataType_Enum dte = hql.getDataType();
				if (dte == DataType_Enum.Date) {
//					query.setParameter(hql.getParamName(), hql
//							.getParamValue(), Hibernate.DateType.);
					query.setDate(hql.getParamName(), (Date) hql
							.getParamValue());
				} else if (dte == DataType_Enum.TimeSamper) {
//					query.setParameter(hql.getParamName(), hql
//							.getParamValue(), Hibernate.TIMESTAMP);
					query.setTimestamp(hql.getParamName(), (Date) hql
							.getParamValue());
				} else if (dte == DataType_Enum.Boolean) {
//					query.setParameter(hql.getParamName(), hql
//							.getParamValue(), Hibernate.BOOLEAN);
					query.setBoolean(hql.getParamName(), Boolean.valueOf(hql.getParamValue().toString()));
				} else if (dte == DataType_Enum.Integer) {
//					query.setParameter(hql.getParamName(), hql
//					.getParamValue(), Hibernate.INTEGER);
					query.setInteger(hql.getParamName(), Integer.valueOf(hql
							.getParamValue().toString()));
				} else if (dte == DataType_Enum.Long) {
//					query.setParameter(hql.getParamName(), hql
//					.getParamValue(), Hibernate.INTEGER);
					query.setLong(hql.getParamName(), Long.valueOf(hql
							.getParamValue().toString()));
				} else {
					query.setParameter(hql.getParamName(), hql
							.getParamValue());
				}
			}
		}
		/* 169 */List l = query.list();
//		tr.commit();
		/* 170 */l = (l == null) ? new ArrayList() : l;
		/* 171 */Long r = Long.valueOf(Long.parseLong(String.valueOf(l.get(0)
				.toString())));
		/* 172 */if (r != null) {
			/* 173 */return r;
			}
		/* 175 */return Long.valueOf(Long.parseLong("0"));
		}

	
	public void batchSave(List<T> entityList)
	{
		Session sesss = getSession();
//		/* 181 */Transaction et = sesss.beginTransaction();
		/* 182 */for (IBaseEntity t : entityList)
			/* 183 */getSession().persist(t);
//		/* 184 */et.commit();
		}

	public Object getSingleValueByCallStoreProctdure(String sp) {
		/* 187 */Query query = getSession().createQuery("{call " + sp + "}");
		/* 188 */return query.uniqueResult();
		}
 
	
	public void clearCache() {
		this.getSession().clear();

	}

	public Session getCurrentSession() {
		return this.getSession();
	}
}
