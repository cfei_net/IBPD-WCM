package com.ibpd.shopping.web.controller.shopInterface;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.exception.IbpdException;
import com.ibpd.henuocms.web.controller.ext.FormModel;
import com.ibpd.henuocms.web.controller.manage.BaseController;
import com.ibpd.shopping.assist.InterfaceUtil;
import com.ibpd.shopping.entity.AccountEntity;
import com.ibpd.shopping.entity.AddressEntity;
import com.ibpd.shopping.service.address.AddressServiceImpl;
import com.ibpd.shopping.service.address.IAddressService;
import com.ibpd.shopping.service.area.AreaServiceImpl;
import com.ibpd.shopping.service.area.IAreaService;
@Controller
public class Inter_Address  extends BaseController{
	@RequestMapping("getAccountAddressList.do")
	public void getAccountAddressList(HttpServletResponse resp,HttpServletRequest req) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		AccountEntity acc=InterfaceUtil.getLoginedAccountInfo(req);
		if(acc==null){
			super.printMsg(resp, "-1", "-1", "没有登录的用户");
			return;
		}
		IAddressService addServ=(IAddressService) getService(AddressServiceImpl.class);
		List<AddressEntity> addList=addServ.getAccountAddressList(acc.getAccount());
		replaceAreaField(addList);
		super.sendJsonByList(resp, addList, Long.valueOf(addList.size()));
	}
	@RequestMapping("getAccountAddress.do")
	public void getAccountAddress(HttpServletResponse resp,HttpServletRequest req,Long id) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		AccountEntity acc=InterfaceUtil.getLoginedAccountInfo(req);
		if(acc==null){
			super.printMsg(resp, "-1", "-1", "没有登录的用户");
			return;
		}
		IAddressService addServ=(IAddressService) getService(AddressServiceImpl.class);
		AddressEntity add=addServ.getEntityById(id);
		if(add==null){
			super.printMsg(resp, "-2", "-2", "没有该地址");
			return;
		}
		List<AddressEntity> addList=new ArrayList<AddressEntity>();
		addList.add(add);
		super.sendJsonByList(resp, addList, Long.valueOf(addList.size()));
	}
	@RequestMapping("saveAddress.do")
	public void saveAddress(HttpServletResponse resp,HttpServletRequest req,@FormModel("address")AddressEntity address) throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		AccountEntity acc=InterfaceUtil.getLoginedAccountInfo(req);
		if(acc==null){
			super.printMsg(resp, "-1", "-1", "没有登录的用户");
			return;
		}
		if(StringUtils.isBlank(address.getName())){
			super.printMsg(resp, "-1", "-1", "收货人姓名不能为空");
			return;
		}
		if(StringUtils.isBlank(address.getProvince())){
			super.printMsg(resp, "-2", "-1", "收货地址-省份不能为空");
			return;
		}
		if(StringUtils.isBlank(address.getCity())){
			super.printMsg(resp, "-3", "-1", "收货地址-城市不能为空");
			return;
		}
		if(StringUtils.isBlank(address.getArea())){
			super.printMsg(resp, "-4", "-1", "收货地址-区县不能为空");
			return;
		}
		if(StringUtils.isBlank(address.getAddress())){
			super.printMsg(resp, "-5", "-1", "收货地址-街道地址不能为空");
			return;
		}
		if(StringUtils.isBlank(address.getZip())){
			super.printMsg(resp, "-7", "-1", "邮政编码不能为空");
			return;
		}
		if(StringUtils.isBlank(address.getPhone())){
			super.printMsg(resp, "-8", "-1", "手机号码不能为空");
			return;
		}
		if(StringUtils.isBlank(address.getMobile())){
			super.printMsg(resp, "-9", "-1", "联系电话不能为空");
			return;
		}
		IAddressService addressServ=(IAddressService) getService(AddressServiceImpl.class);
		if(address.getId()==null || address.getId()==-1L){
			address.setAccount(acc.getAccount());
			address.setIsdefault("n");
			addressServ.saveEntity(address);
		}else{
			AddressEntity add=addressServ.getEntityById(address.getId());
			String account=add.getAccount();
			swap(address,add);
			add.setAccount(account);
			addressServ.saveEntity(add);
		}
		super.printMsg(resp, "0", "0", "保存成功");
	}
	@RequestMapping("setDefaultAddress.do")
	public void setDefaultAddress(HttpServletResponse resp,HttpServletRequest req,Long id) throws IOException{
		IAddressService addressServ=(IAddressService) getService(AddressServiceImpl.class);
		AddressEntity address=addressServ.getEntityById(id);
		if(address==null){
			super.printMsg(resp, "-1", "-1", "没有该实体");
			return;
		}
		String account=address.getAccount();
		List<AddressEntity> addList=addressServ.getAccountAddressList(account);
		for(AddressEntity add:addList){
			add.setIsdefault("n");
			addressServ.saveEntity(add);
		}
		address.setIsdefault("y");
		addressServ.saveEntity(address);
		super.printMsg(resp, "0", "0", "设置为默认成功");
	}
	@RequestMapping("removeAddress.do")
	public void removeAddress(HttpServletResponse resp,HttpServletRequest req,Long id) throws IOException{
		IAddressService addressServ=(IAddressService) getService(AddressServiceImpl.class);
		addressServ.deleteByPK(id);
		super.printMsg(resp, "0", "0", "执行成功");
	}

	private void replaceAreaField(List<AddressEntity> addList){
		IAreaService areaServ=(IAreaService) getService(AreaServiceImpl.class);
		for(AddressEntity add:addList){
			try {
				add.setProvince(areaServ.getAreaNameByCode(add.getProvince()));
				add.setCity(areaServ.getAreaNameByCode(add.getCity()));
				add.setArea(areaServ.getAreaNameByCode(add.getArea()));
			} catch (IbpdException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
