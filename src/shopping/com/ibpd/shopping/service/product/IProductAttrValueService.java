package com.ibpd.shopping.service.product;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.ProductAttrValueEntity;

public interface IProductAttrValueService extends IBaseService<ProductAttrValueEntity> {
	List<ProductAttrValueEntity> getListByProductId(Long productId);
	List<ProductAttrValueEntity> getAttributeLinkList(Long attributeId,Long productId);

	
}
