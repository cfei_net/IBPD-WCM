package com.ibpd.shopping.service.orderDetail;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.OrderdetailEntity;

public interface IOrderdetailService extends IBaseService<OrderdetailEntity> {
	List<OrderdetailEntity> getListByOrderId(Long orderId);
	void deleteByOrderId(Long orderId);
}
