package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.assist.PermissionModelEntity;
import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.entity.ContentEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.service.content.ContentServiceImpl;
import com.ibpd.henuocms.service.content.IContentService;

@Controller
public class RecycleContent extends BaseController {
	@RequestMapping("Manage/RecycleContent/index.do")
	public String index(org.springframework.ui.Model model,HttpServletRequest req,String nodeId) throws IOException{
		model.addAttribute(PAGE_TITLE,"内容回收站");
		model.addAttribute("nodeId",nodeId);
		if(nodeId.indexOf("_")==-1){
			model.addAttribute("cont_recycle_restore",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_recycle_restore", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("cont_recycle_del",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_recycle_del", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_NODE));
			model.addAttribute("cont_recycle_reload",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_recycle_reload", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_NODE));
		}else{
			nodeId=nodeId.split("_")[1];
			model.addAttribute("cont_recycle_restore",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_recycle_restore", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("cont_recycle_del",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_recycle_del", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_SITE));
			model.addAttribute("cont_recycle_reload",SysUtil.checkPermissionIsEnable(req.getSession(), "cont_recycle_reload", Long.valueOf(nodeId),PermissionModelEntity.MODEL_TYPE_SITE));
		}
		return "manage/recyclecontent/index";
	}
	@RequestMapping("Manage/RecycleContent/doRestore.do")
	public void doRestore(String ids,HttpServletResponse resp) throws IOException{
		IContentService nodeService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
		if(StringUtils.isBlank(ids)){
			super.printMsg(resp, "-1", "-1", "参数缺失");
			return;
		}
		String[] idss=ids.split(",");
		for(String id:idss){
			if(StringUtils.isNumeric(id)){
				ContentEntity n=nodeService.getEntityById(Long.valueOf(id));
				if(n!=null){
					n.setDeleted(false);
					nodeService.saveEntity(n);
				}
			} 
		}
		super.printMsg(resp, "1", "1", "保存成功");
	}
	@RequestMapping("Manage/RecycleContent/doDel.do")
	public void doDel(String ids,HttpServletResponse resp) throws IOException{
		IContentService nodeService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
		nodeService.batchDel(ids);
		super.printMsg(resp, "1", "1", "保存成功");
	}
	@RequestMapping("Manage/RecycleContent/list.do")
	public void getList(String id,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryString,HttpServletRequest req){
		IContentService nodeService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
		String ss[]=id.split("_");
		String query="";
		if(ss.length==2){
			query="subSiteId="+ss[1]+"";
		}else{
			query="nodeIdPath like '%,"+id+",%'";
		}
		if(queryString!=null && !queryString.trim().equals("")){
			query+=" and text like '%"+queryString.trim()+"%'";
		}
		
		query+=" and deleted=true"; 
//		nodeService.getDao().clearCache();
		super.getList(req, nodeService, resp, order, page, rows, sort, query);
	}
	@RequestMapping("Manage/RecycleContent/props.do")
	public String props(Long id,Model model) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(id==null || id<0L){
			model.addAttribute(ERROR_MSG,MSG_PARAMERROR_MSG);
			return super.ERROR_PAGE;
		}
		IContentService nodeService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
		ContentEntity ce=nodeService.getEntityById(id);
		if(ce==null){
			model.addAttribute(ERROR_MSG,"没有该栏目");
			return super.ERROR_PAGE;
		}
		model.addAttribute(PAGE_TITLE,"参数设置");
		model.addAttribute("htmls",super.getHtmlString(ce, "recyclecontent.edit.field"));
		model.addAttribute("entity",ce);
		return "manage/recyclecontent/props";
	}	
	@RequestMapping("Manage/RecycleContent/saveProp.do")
	public void saveProps(Long id,String field,String value,HttpServletResponse resp) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException{
		if(id!=null && id!=-1){
			IContentService nodeService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
			ContentEntity cata=nodeService.getEntityById(id);
				if(cata!=null){
					//先确定参数类型
					Method tmpMethod=cata.getClass().getMethod("get"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{});
					
					
					Method method=cata.getClass().getMethod("set"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{tmpMethod.getReturnType()});
					Object val=getValue(value,tmpMethod.getReturnType().getSimpleName());
					IbpdLogger.getLogger(this.getClass()).info("value="+val);
					method.invoke(cata, val);
					nodeService.saveEntity(cata);
//					nodeService.getDao().clearCache();
					super.printMsg(resp, "99", cata.getId().toString(), "保存成功");		
				}else{
					super.printMsg(resp, "-1", "", "没有该类别");
				}
		}else{
			printParamErrorMsg(resp);			
		}
	}
}
