package com.ibpd.sysinfo;

public interface IMonitorService {
	public MonitorInfoBean getMonitorInfoBean() throws Exception;
}